import { urlParams } from './url'

var DIAMONDS = {
  common: {
    init: function () {
      // application wide code
    }
  },
  register: {
    init: function () {
      // set token input to "?token=" query parameter
      document.getElementById('token').value = urlParams.get('token')

      // html5 validators
      var username = document.getElementById('username')
      var password = document.getElementById('password')
      var confirmPassword = document.getElementById('confirm_password')
      var token = document.getElementById('token')

      username.addEventListener('input', () => {
        if (username.validity.typeMismatch) {
          username.setCustomValidity('format: @username:dmnd.sh')
        } else {
          username.setCustomValidity('')
        }
      })

      token.addEventListener('input', () => {
        if (token.validity.typeMismatch) {
          token.setCustomValidity('case-sensitive, e.g: SardineImpactReport')
        } else {
          token.setCustomValidity('')
        }
      })

      function validatePassword () {
        if (password.value !== confirmPassword.value) {
          confirmPassword.setCustomValidity("passwords don't match")
        } else {
          confirmPassword.setCustomValidity('')
        }
      }

      confirmPassword.addEventListener('input', () => validatePassword())

      password.addEventListener('input', () => {
        validatePassword()
        if (password.validity.typeMismatch) {
          password.setCustomValidity('atleast 8 characters long')
        } else {
          password.setCustomValidity('')
        }
      })

      const form = document.getElementById('registration')
      const success = document.getElementById('success')
      const welcome = document.getElementById('welcome')

      function sendData () {
        const XHR = new window.XMLHttpRequest()

        // Bind the FormData object and the form element
        const FD = new window.FormData(form)

        // Define what happens on successful data submission
        XHR.addEventListener('load', () => {
          const response = JSON.parse(XHR.responseText)
          const welcomeMsg = 'Welcome ' + response.user_id + ', to diamonds!'

          console.log(response)
          if (response.status_code === 200) {
            form.classList.toggle('hidden')
            success.classList.toggle('hidden')
            welcome.innerHTML = welcomeMsg
          } else {
            window.alert(response.error)
          }
        })

        // Define what happens in case of error
        XHR.addEventListener('error', () => {
          window.alert('an internal error occured!')
        })

        // Set up our request
        const endpoint = window.location.protocol + '//' + window.location.host + '/register'
        XHR.open('POST', endpoint)

        // The data sent is what the user provided in the form
        XHR.send(FD)
      }

      // take over its submit event.
      form.addEventListener('submit', (event) => {
        event.preventDefault()

        sendData()
      })
    }
  }
}

// based on https://www.viget.com/articles/extending-paul-irishs-comprehensive-dom-ready-execution/
var UTIL = {
  exec: function (controller, action) {
    var ns = DIAMONDS

    action = (action === undefined) ? 'init' : action
    if (controller !== '' && ns[controller] && typeof ns[controller][action] === 'function') {
      ns[controller][action]()
    }
  },
  init: function () {
    var body = document.body
    var controller = body.getAttribute('data-controller')
    var action = body.getAttribute('data-action')
    UTIL.exec('common')
    UTIL.exec(controller)
    UTIL.exec(controller, action)
  }
}

window.onload = function () {
  UTIL.init()
}
