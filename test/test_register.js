/* eslint-env mocha */
/* global fixture */
import { expect } from 'chai'

describe('test register page', () => {
  // If base path is different from the default `spec/fixtures`
  before(() => {
    fixture.setBase('dist')
  })

  beforeEach(() => {
    fixture.load('register.html')
  })

  afterEach(() => {
    fixture.cleanup()
  })

  describe('test username input', () => {
    const usernames = [
      { username: 'zeratax', expected: true },
      { username: '@ZerataX', expected: true },
      { username: '@ZerataX:dmnd.sh', expected: true },
      { username: 'ZerataX@dmnd.sh', expected: false }
    ]

    usernames.forEach((test) => {
      it(`username ${test.username} should be ${(test.expected) ? 'correct' : 'incorrect'}`, () => {
        const userInput = window.document.getElementById('username')
        userInput.value = test.username
        expect(userInput.checkValidity()).to.equal(test.expected)
      })
    })
  })

  describe('test password input length', () => {
    const passwords = [...Array(9)].map((_, i) => [...Array(i).keys()].toString().replace(/,/g, ''))

    passwords.forEach((password) => {
      it(`password ${password} should be ${(password.length >= 8) ? 'correct' : 'incorrect'}`, () => {
        const passwordInput = window.document.getElementById('password')
        passwordInput.value = password
        expect(passwordInput.checkValidity()).to.equal(password.length >= 8)
      })
    })
  })

  describe.skip('test confirm password input', () => {
    const passwords = [
      { password: 'foo', repeat: 'bar', expected: false },
      { password: 'foo', repeat: 'foo', expected: true }
    ]

    passwords.forEach((test) => {
      it(`password ${test.password} and repeated password ${test.repeat} are ${(test.expected) ? 'the same' : 'not the same'}`, () => {
        /* eslint-disable no-unused-expressions */
        const passwordInput = window.document.getElementById('password')
        const repeatInput = window.document.getElementById('confirm_password')

        passwordInput.value = test.password
        repeatInput.value = ''
        expect(repeatInput.checkValidity()).to.false

        passwordInput.value = test.password
        repeatInput.value = test.repeat
        expect(repeatInput.checkValidity()).to.equal(test.expected)

        passwordInput.value = ''
        repeatInput.value = test.repeat
        expect(repeatInput.checkValidity()).to.false

        passwordInput.value = test.password
        repeatInput.value = test.repeat
        expect(repeatInput.checkValidity()).to.equal(test.expected)
        /* eslint-enable no-unused-expressions */
      })
    })
  })

  describe('test token input', () => {
    const tokens = [
      { token: 'DoubleWizardSky', expected: true },
      { token: 'Double Wizard Sky', expected: false },
      { token: 'doublewizardsky', expected: false }
    ]

    tokens.forEach((test) => {
      it(`token ${test.token} should be ${(test.expected) ? 'correct' : 'incorrect'}`, () => {
        const tokenInput = window.document.getElementById('token')
        tokenInput.value = test.token
        expect(tokenInput.checkValidity()).to.equal(test.expected)
      })
    })
  })
})
