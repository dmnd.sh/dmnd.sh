/* eslint-env mocha */
import { expect } from 'chai'
import diamond from '../src/scripts/diamond.js'

describe('test', () => {
  describe('#test()', () => {
    it('this is just a test test', () => {
      expect(diamond).to.equal('DIAMONDS')
    })
  })
})

// TODO phantom-js tests
// https://github.com/nathanboktae/mocha-phantomjs
