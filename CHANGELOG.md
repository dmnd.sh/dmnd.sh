# Changelog
## dmnd.sh v0.0.4 (2020-03-20)

### Features
-  minecraft section
-  register page tests

### Changes
- travis ci => gitlab ci

## dmnd.sh v0.0.3 (2018-06-18)
### Features
-  add first pages

## dmnd.sh v0.0.2 (2018-04-23)
### Features
-  add tests and linting

## dmnd.sh v0.0.1 (2018-04-23)
-  initial project structure
