[![pipeline status](https://gitlab.com/dmnd.sh/dmnd.sh/badges/master/pipeline.svg)](https://gitlab.com/dmnd.sh/dmnd.sh/-/commits/master) [![coverage report](https://gitlab.com/dmnd.sh/dmnd.sh/badges/master/coverage.svg)](https://gitlab.com/dmnd.sh/dmnd.sh/-/commits/master)


# dmnd.sh

[dmnd.sh](https://dmnd.sh) main website

## build

```
  npm install
  npm run deploy
```

and then put `dist/` in your public nginx folder or w/e.

## develop

to have the system rebuild on file change and reload your browser execute these two commands in different shells.
```
  npm run watch
  npm run live
```

## contribute

if you want to contribute please install the pre-commit script, execute the following scripts in the root of the repository
```
  ln -s ../../scripts/pre-commit.sh .git/hooks/pre-commit
  chmod +x pre-commit.sh
```

now everytime you commit the project will be linted and tested.
