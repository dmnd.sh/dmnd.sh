// process.env.CHROME_BIN = require('puppeteer').executablePath()
process.env.CHROME_BIN = process.env.CHROME_BIN || require('puppeteer').executablePath()

module.exports = function (config) {
  config.set({
    files: [
      {
        pattern: './dist/**/*'
      },
      './src/scripts/**/*.js',
      './test/**/*.js'
    ],
    // exclude: [
    //   './test/**/*_sass.js'
    // ],

    frameworks: [
      'browserify',
      'mocha',
      'chai',
      'fixture'
    ],
    plugins: [
      'karma-browserify',
      'karma-chrome-launcher',
      'karma-coverage',
      'karma-chai',
      'karma-fixture',
      'karma-html2js-preprocessor',
      'karma-htmlfile-reporter',
      'karma-mocha',
      'karma-mocha-reporter'
    ],

    preprocessors: {
      './src/scripts/**/*.js': ['browserify'],
      './test/**/*.js': ['browserify'],
      './dist/**/*.html': ['html2js']
    },
    browserify: {
      debug: true,
      extensions: ['.js'],
      transform: [
        ['babelify', { presets: ['@babel/preset-env'], plugins: ['istanbul'] }]
      ]
    },

    reporters: [
      'coverage',
      'mocha',
      'progress',
      'html'
    ],
    coverageReporter: {
      reporters: [
        {
          type: 'text'
        }, {
          type: 'html',
          dir: 'coverage',
          subdir: 'html'
        }, {
          type: 'lcovonly',
          dir: 'coverage',
          subdir: 'lcov'
        }]
    },
    htmlReporter: {
      outputFile: 'coverage/units.html'
    },

    browsers: ['Chrome', 'ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-gpu'
        ]
      }
    },
    browserDisconnectTimeout: 10000,
    browserDisconnectTolerance: 3,
    browserNoActivityTimeout: 60000,
    autoWatch: false
    // concurrency: Infinity
  })
}
